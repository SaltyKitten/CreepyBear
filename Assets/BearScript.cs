﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BearScript : MonoBehaviour {


    public HatScript hatPrefab;
    public HatScript flyingHat;
    public SpriteRenderer bearSprite;
    public Sprite[] sprites;
    public Transform hatSpawner;


    public AnimatorOverrideController noHatAnimator;


    private Vector2 _firstMousePosition;
    private float   _timeMousePosition;
    private const float DEPLACEMENT_CONST = 0.05f;
    private Animator _animator;
    private AnimatorOverrideController _hatAnimator;

    private Vector3 _move;

    // Update is called once per frame
    void Update() {

        if (Input.GetMouseButtonDown(0)) {
            _firstMousePosition = Input.mousePosition;
            _timeMousePosition  = Time.time;
        }
        else if (!flyingHat && Input.GetMouseButtonUp(0)) {
            Vector2 throwVector = new Vector2(Input.mousePosition.x - _firstMousePosition.x,Input.mousePosition.y - _firstMousePosition.y);
            float speed =  throwVector.magnitude/200 / ((Time.time-_timeMousePosition)*2);
            if (speed >0) {
                flyingHat = Instantiate(hatPrefab).GetComponent<HatScript>();
                flyingHat.Init(this);
                flyingHat.transform.position = hatSpawner.position;
                flyingHat.Throw(throwVector.x / throwVector.magnitude, throwVector.y / throwVector.magnitude, speed);
                UpdateSprite();

            }
        }

        if (Input.GetMouseButtonDown(1)) {
            if (flyingHat != null)
                flyingHat.GetBack();


        }

        //move
        _move = new Vector3();
        if (Input.GetKey(KeyCode.Z)) {
            _move.y = _move.y + DEPLACEMENT_CONST;
            bearSprite.sprite = flyingHat == null ? sprites[6] : sprites[2];
        }
        else if (Input.GetKey(KeyCode.S)) {
            _move.y = _move.y - DEPLACEMENT_CONST;
            bearSprite.sprite = flyingHat == null ? sprites[4] : sprites[0];
        }
        else if (Input.GetKey(KeyCode.Q)) {
            _move.x = _move.x - DEPLACEMENT_CONST;
            bearSprite.sprite = flyingHat == null ? sprites[7] : sprites[3];
        }
        else if (Input.GetKey(KeyCode.D)) {
            _move.x = _move.x + DEPLACEMENT_CONST;
            bearSprite.sprite = flyingHat == null ? sprites[5] : sprites[1];

        }
        UpdateSprite();
        GetComponent<Rigidbody2D>().MovePosition(transform.position + _move);
    }

    public void UpdateSprite() {
        if (flyingHat)
            _animator.runtimeAnimatorController = noHatAnimator;
        else
            _animator.runtimeAnimatorController = _hatAnimator;

        if (_move.y >0) {
            _animator.SetTrigger("Back");
            bearSprite.flipX = false;
        }
        else if (_move.x <0) {
            _animator.SetTrigger("Side");
            bearSprite.flipX = true;
        }
        else if (_move.x >0) {
            _animator.SetTrigger("Side");
            bearSprite.flipX = false;
        }
        else if (_move.y <0) {
            _animator.SetTrigger("Front");
            bearSprite.flipX = false;
        }
        else {
            _animator.SetTrigger("Iddle");
            bearSprite.flipX = false;
        }

    }

    private void Awake() {
        _animator = GetComponent<Animator>();
        _hatAnimator = new AnimatorOverrideController(_animator.runtimeAnimatorController);
    }
}


