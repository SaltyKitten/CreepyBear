﻿using System;
using UnityEngine;

public class HatScript : MonoBehaviour {

    public float speed;
    public float angle;

    private BearScript _parentBear;
    private bool _isComingBack;
    private bool _justSpawn = true;
    private Vector2 _direction;

    // Use this for initialization
    void Start() {


    }

    // Update is called once per frame
    void Update() {
        if (_isComingBack)
            GetBack();
        transform.Translate(_direction*speed*Time.deltaTime);

    }
    private void OnCollisionEnter2D(Collision2D collision) {
        if (!_isComingBack) {
            if (collision.gameObject.GetComponent<WallScript>() != null) { // todo : replace with layer ?
                ContactPoint2D[] collisions = new ContactPoint2D[10];
                _direction =  Vector2.Reflect(_direction, collision.contacts[0].normal);
                Debug.DrawRay(collisions[0].point, collisions[0].normal, Color.red, 2f);
                speed = speed/2f;
            }
        }
        if (collision.gameObject.layer == LayerMask.NameToLayer("HatSpawner")) {

            if (_justSpawn)
                _justSpawn = false;
            else {

                _parentBear.flyingHat = null;
                _parentBear.UpdateSprite();
                Destroy(this.gameObject);
            }
        }


    }

    public void Init(BearScript parent) {
        _parentBear =parent;
    }

    public void Throw(float x, float y, float speed) {
        _direction =  new Vector2(x, y);
        this.speed = speed;
    }

    internal void GetBack() {
        _isComingBack = true;
        _justSpawn = false; //hack
        Vector2 direction = new Vector2(_parentBear.hatSpawner.transform.position.x - transform.position.x,_parentBear.hatSpawner.transform.position.y - transform.position.y).normalized;
        Throw(direction.x, direction.y, speed);
    }
}
